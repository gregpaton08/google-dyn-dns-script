#!/bin/bash

#
# Use this script to update a dynamic dns entry pointing at 'madsonic'
# run 'sudo crontab -e' and add the entry:
#     '* * * * * bash /home/pi/scripts/google-dyn-dns-script/update_google_dns.sh'
#

get_external_ip() {
    #
    # Prints just the external ip address assigned to this vm or computer.
    # Found at:
    #      http://osxdaily.com/2013/07/16/quick-external-ip-address-command-line/
    #
    # dig +short myip.opendns.com @resolver1.opendns.com

    curl https://ipinfo.io/ip
}


compare_ips() {
    #
    # Compare the current external ip address with the previous ip address
    #
    local curr_ip="$1"
    local prev_ip="$2"
    [[ -n "$curr_ip" ]] && [[ -n "$prev_ip" ]] && [[ "$curr_ip" = "$prev_ip" ]]  # strings not empty and they equal
}


update_google() {
    #
    # We update google if the previous and current ips do not match
    #
    username_file="username"
    if [ ! -f $username_file ]; then
        echo "Please create a file named \"username\" containing the username for the account"
        exit 1
    fi
    declare -r uname=$(cat $username_file)

    password_file="password"
    if [ ! -f $password_file ]; then
        echo "Please create a file named \"password\" containing the password for the account"
        exit 1
    fi
    declare -r pword="$(cat $password_file)"

    hostname_file="hostname"
    if [ ! -f $hostname_file ]; then
        echo "Please create a file named \"hostname\" containing the hostname for the account"
        exit 1
    fi
    my_host="$(cat $hostname_file)"

    user_agent="curl"

    url="https://${uname}:${pword}@domains.google.com/nic/update?hostname=$my_host"

    echo $url

    response=""
    while [[ -z "$response" ]] || [[ "$response" = '911' ]]; do
        if [[ "$response" = '911' ]]; then
            # per google: sleep for 5 minutes
            sleep 5m
        fi
        response=$(curl -s --user-agent $user_agent $url --ssl-reqd)
    done

    echo $response
}

# prev_ip_file="$(find /root -name 'previous_ip.txt')"
prev_ip_file="previous_ip.txt"
main() {
    # navigated to the directory of this script
    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    cd ${DIR}

    local current_ip=$(get_external_ip)
    local previous_ip="$(cat $prev_ip_file 2>/dev/null)"

    if [ -z "$previous_ip" ]; then
        touch "$prev_ip_file"
    fi

    if ! compare_ips $current_ip $previous_ip; then
        update_google
        echo $current_ip > "$prev_ip_file"
    fi
}


# Run the script
main
